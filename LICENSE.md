# GNU General Public License (GPL)

## Language specs

- actionscript.lang
- ada.lang
- apache.lang
- asciidoc.lang
- asp.lang
- automake.lang
- awk.lang
- bennugd.lang
- bibtex.lang
- boo.lang
- cfml.lang
- cg.lang
- changelog.lang
- chdr.lang
- c.lang
- cmake.lang
- cobol.lang
- coffee.lang
- coffee_script.lang
- cpp.lang
- csharp.lang
- cson.lang
- css.lang
- cucumber.lang
- cuda.lang
- def.lang
- desktop.lang
- diff.lang
- d.lang
- docbook.lang
- dosbatch.lang
- dot.lang
- dpatch.lang
- dtd.lang
- eiffel.lang
- erlang.lang
- fcl.lang
- forth.lang
- fortran.lang
- fsharp.lang
- gap.lang
- gdb-log.lang
- glsl.lang
- go.lang
- gtk-doc.lang
- gtkrc.lang
- haddock.lang
- haskell.lang
- haskell-literate.lang
- html.lang
- idl.lang
- imagej.lang
- ini.lang
- java.lang
- javascript.lang
- j.lang
- json.lang
- latex.lang
- less.lang
- libtool.lang
- literate_coffee.lang
- literate_coffee_script.lang
- lua.lang
- m4.lang
- makefile.lang
- mako.lang
- mallard.lang
- markdown.lang
- matlab.lang
- mxml.lang
- nemerle.lang
- netrexx.lang
- nsis.lang
- objc.lang
- objj.lang
- ocaml.lang
- ocl.lang
- octave.lang
- ooc.lang
- opal.lang
- opencl.lang
- pascal.lang
- perl.lang
- php.lang
- pkgconfig.lang
- po.lang
- prolog.lang
- protobuf.lang
- puppet.lang
- python3.lang
- python.lang
- R.lang
- rpmspec.lang
- ruby-bundler-gemfile-lock.lang
- ruby.lang
- ruby_on_rails.lang
- scheme.lang
- scilab.lang
- sh.lang
- sml.lang
- sparql.lang
- sql.lang
- systemverilog.lang
- t2t.lang
- tcl.lang
- texinfo.lang
- vala.lang
- vbnet.lang
- verilog.lang
- vhdl.lang
- xml.lang
- xslt.lang
- yacc.lang
- yml.lang

## Styles

- Active4D.xml
- Airkitek.xml
- All Hallow's Eve.xml
- Amy.xml
- Argonaut.xml
- barf.xml
- BBEdit.xml
- Bespin.xml
- Blackboard.xml
- Black Pearl II.xml
- Black Pearl.xml
- blue_dream.xml
- BoysAndGirls01.xml
- Brilliance Black.xml
- Brilliance Dull.xml
- build.xml
- chela_light.xml
- Choco.xml
- Classic Modified.xml
- cobalt.xml
- codezone.xml
- CodeZone.xml
- Cool Glow.xml
- Daltonism.xml
- darkmacs.xml
- darkmate.xml
- Dawn.xml
- daylight.xml
- Django (Smoothy).xml
- Django.xml
- dreamweaver.xml
- eclips3.media (ECLM).xml
- Eiffel.xml
- emacs_dark.xml
- emacs-dark.xml
- emacs_mod.xml
- emacs.xml
- Emacs.xml
- espresso_libre.xml
- Espresso Libre.xml
- Fade to Grey.xml
- ForLaTeX.xml
- Frill.xml
- GitHub.xml
- GlitterBomb.xml
- GmateLight.xml
- Gmate.xml
- greenscreen.xml
- idleFingers.xml
- idle.xml
- IDLE.xml
- iLife 05.xml
- inkpot.xml
- inkpot.xml.1
- iPlastic.xml
- IR_Black.xml
- IR_White.xml
- jellybeans.xml
- LAZY.xml
- Lowlight.xml
- Mac Classic.xml
- MagicWB (Amiga).xml
- Matrix.xml
- Merbivore Soft.xml
- Merbivore.xml
- Midnight.xml
- minimal_Theme.xml
- Mono Industrial.xml
- monokai-extended.xml
- Monokai.xml
- mustang.xml
- Neopro.xml
- Notepad2.xml
- Pastels on Dark.xml
- PhilGeek.xml
- PlasticCodeWrap.xml
- Plum Dumb.xml
- Pythonic.xml
- Railscasts black improved.xml
- Railscasts black.xml
- Railscasts improved.xml
- Railscasts.xml
- RDark.xml
- Ruby Blue.xml
- Ruby Robot.xml
- RubyRobot.xml
- ryan-light.xml
- slate.xml
- Slate.xml
- slush_and_poppies.xml
- Slush & Poppies.xml
- Smurfy.xml
- SpaceCadedPro.xml
- SpaceCadet.xml
- Spectacular.xml
- Stoneship.xml
- Sunburst.xml
- Swyphs II.xml
- Tango.xml
- Tek.xml
- Text Ex Machina.xml
- textmate.xml
- Tinge.xml
- Tomorrow-Night-Blue.xml
- Tomorrow-Night-Bright.xml
- tomorrow_night-eighties.xml
- Tomorrow-Night-Eighties.xml
- Tomorrow-Night.xml
- Tomorrow.xml
- Travis Jeffery.xml
- turbo.xml
- twilight_mod.xml
- twilight.xml
- Twilight.xml
- vibrant_fun.xml
- Vibrant Ink.xml
- vibrant_nerd.xml
- vitamins.xml
- Whys Poignant.xml
- Zenburnesque.xml

# MIT license

## Language Specs

- coco.lang
