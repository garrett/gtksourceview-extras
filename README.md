# GtkSourceView syntax highlighting & themes

For use with GNOME Builder and GEdit

## Installation

Run the following in a terminal:

```
git clone git@gitlab.com:garrett/gtksourceview-extras.git ~/.local/share/gtksourceview-4/
```

Restart any apps that use GtkSourceView, such as GNOME Builder and GEdit, and you should see improved syntax highlighting.

## Sources

Most from:
https://github.com/ivyl/gedit-mate

Additional files from:
https://github.com/janlelis/rubybuntu-language-specs

Lots of additional files from:
https://github.com/AndersDJohnson/gtksourceview-files

CoffeeScript & literate CoffeeScript:
https://github.com/wavded/gedit-coffeescript

React from:
https://github.com/gdrius/react-jsx-language-specs

Themes included from GNOME Wiki:
https://wiki.gnome.org/Projects/GtkSourceView/StyleSchemes

And, finally, gmate (lang & themes) layered on top:
https://github.com/gmate/gmate
